﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop_unitype_2019
{
    public abstract class AbstractDifficultyImpl : IDifficulty
    {
        private static readonly double BASE_SPEED = 0.05;
        private static readonly int DEFAULT_SHORT_WORDS = 2;
        private static readonly int DEFAULT_LONG_WORDS = 1;

        private double speedOffset;

        public AbstractDifficultyImpl()
        {
            speedOffset = 0;
            NShort = DEFAULT_SHORT_WORDS;
            NLong = DEFAULT_LONG_WORDS;
        }

        /// <summary>    
        /// Property <c>MinSpeed</c> gives the minimum speed the words of the wave can assume. 
        /// </summary>
        public double MinSpeed
        {
            get { return BASE_SPEED; }
        }

        /// <summary>    
        /// Property <c>MaxSpeed</c> gives the maximum speed the words of the wave can assume. 
        /// </summary>
        public double MaxSpeed
        {
            get { return BASE_SPEED + speedOffset; }
        }

        /// <summary>
        /// Property <c>NShort</c> gives the number of short words composing the wave.
        /// </summary>
        public int NShort { get; private set; }

        /// <summary>
        ///Property NLong gives the number of long words composing the wave
        /// </summary>
        public int NLong { get; private set; }

        /**
         * method to bring offset to its initial value.
         */
        protected void ResetOffset()
        {
            speedOffset = 0;
        }

        /**
         * method to bring number of short words to its initial value.
         */
        protected void ResetnShort()
        {
            NShort = DEFAULT_SHORT_WORDS;
        }

        /**
         * method to bring number of long words to its initial value.
         * 
         */
        protected void ResetnLong()
        {
            NLong = DEFAULT_LONG_WORDS;
        }

        /**
         * Method to increase the range of value for the speed.
         * 
         * @param n
         *              the increase
         */
        protected void IncreaseOffset(double n)
        {
            speedOffset = speedOffset + n;
        }

        /**
         * Method to increase the number of short words by 1.
         */
        protected void IncrementShort()
        {
            NShort = NShort + 1;
        }

        /// <summary>
        /// Method to increase the number of long words by 1.
        /// </summary>
        protected void IncrementLong()
        {
            NLong = NLong + 1;
        }

        /// <summary>    
        /// method <c>Increase</c> is used to increase the difficulty. 
        /// </summary>
        public abstract void Increase();
    }
}
