﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop_unitype_2019
{

    public class SimpleIncreasingDifficulty : AbstractDifficultyImpl
    {

    private bool shortTurn = true;
    private readonly ISpeedOffsetIncrement increment;
    private readonly int limit;

        public SimpleIncreasingDifficulty(int limit, ISpeedOffsetIncrement increment) : base()
        {
            this.limit = limit;
            this.increment = increment;
        }

        /// <summary>    
        /// method <c>Increase</c> is used to increase the difficulty. 
        /// </summary>
        public override void Increase()
            {
                IncreaseOffset(increment.Increment);

                if (!IsLimitReached())
                {
                    if (shortTurn)
                    {
                        IncrementShort();

                    }
                    else
                    {
                        IncrementLong();
                    }

                    this.shortTurn = !this.shortTurn;
                }

            }

        private bool IsLimitReached()
        {
            return NLong + NShort >= limit;
        }


    }

}
