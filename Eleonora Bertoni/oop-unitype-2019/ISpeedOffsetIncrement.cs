﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop_unitype_2019
{
    public interface ISpeedOffsetIncrement
    {   
        /// <summary>
        /// Property <c>Increment</c> gives the next increment.
        /// </summary>
        double Increment
        {
            get;
        }
    }
}
