﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop_unitype_2019
{

    /// <summary>
    /// <c>IDifficulty</c>
    /// represents the increasing difficulty of the game determined by the 
    /// number of the words of the wave and their related speed.
    /// </summary>
    public interface IDifficulty
    {
        /// <summary>    
        /// method <c>Increase</c> is used to increase the difficulty. 
        /// </summary>
        void Increase();


        /// <summary>    
        /// Property <c>MinSpeed</c> gives the minimum speed the words of the wave can assume. 
        /// </summary>
        double MinSpeed
        {
            get;
        }

        /// <summary>    
        /// Property <c>MaxSpeed</c> gives the maximum speed the words of the wave can assume. 
        /// </summary>
        double MaxSpeed
        {
            get;
        }

        /// <summary>
        /// Property <c>NShort</c> gives the number of short words composing the wave.
        /// </summary>
        int NShort
        {
            get;
        }

        /// <summary>
        ///Property NLong gives the number of long words composing the wave
        /// </summary>
        int NLong
        {
            get;
        }
    }

}
