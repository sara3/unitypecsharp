﻿using System.Collections;
using System.Collections.Generic;

namespace oop_unitype_2019
{
    /// <inheritdoc/>
    public class DiffValSpeedOffsetIncrement : ISpeedOffsetIncrement
    {
        private IEnumerator<double> increments;

        public DiffValSpeedOffsetIncrement()
        {
            increments = new MyEnumerator();
        }

        /// <summary>
        /// Property <c>Increment</c> gives the next increment.
        /// </summary>
        public double Increment
        {  
            get { return increments.Current; }
        }

    
}

    class MyEnumerator : IEnumerator<double>
    {

        private static IList<double> values;
        private IEnumerator<double> it;
        public MyEnumerator()
        {
            values = new List<double> { 0.1, 0.07, 0.06, 0.03 };

            it = values.GetEnumerator();

        }

        public double Current
        {
            get
            {
                if (!MoveNext())
                {
                    Reset();
                    MoveNext();
                }
                return it.Current;
            }
        }

        object IEnumerator.Current => it.Current;

        public void Dispose()
        {
            it.Dispose();
        }

        public bool MoveNext()
        {
            return it.MoveNext();
        }

        public void Reset()
        {
            it.Reset();
        }
    }
}


