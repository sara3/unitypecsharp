﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using oop_unitype_2019;

namespace test
{
    [TestClass]
    public class DifficultyTest
    {
        private const int limit = 10;
        private ISpeedOffsetIncrement speedIncrement;
        private IDifficulty difficulty;

        /// <summary>
        /// Used to initialize fields for each test
        /// </summary>
        [TestInitialize]
        public void TestInitialize()
        {
            speedIncrement = new DiffValSpeedOffsetIncrement();
            difficulty = new SimpleIncreasingDifficulty(limit, speedIncrement);

        }

        /// <summary>
        /// Used to test the speed increment 
        /// </summary>
        [TestMethod]
        public void TestDiffValSpeedOffsetIncrement()
        {
            for (int i = 0; i < limit; i++)
            {   
                Assert.IsTrue(speedIncrement.Increment == 0.1);
                Assert.IsTrue(speedIncrement.Increment == 0.07);
                Assert.IsTrue(speedIncrement.Increment == 0.06);
                Assert.IsTrue(speedIncrement.Increment == 0.03);
            }

        }
        /// <summary>
        /// Used to test the difficulty increment
        /// </summary>
        [TestMethod]
        public void TestDifficulty()
            {
                int tempShort = difficulty.NShort;
                int tempLong = difficulty.NLong;
                double tempSpeed = difficulty.MaxSpeed;
                bool turn = true;

                for (int i = tempShort + tempLong; i < limit; i++)
                {
                    difficulty.Increase();
                    if (turn)
                    {
                        Assert.IsTrue(difficulty.NShort == tempShort + 1);
                        Assert.IsTrue(difficulty.NLong == tempLong);
                        tempShort++;
                    }
                    else
                    {
                        Assert.IsTrue(difficulty.NShort == tempShort);
                        Assert.IsTrue(difficulty.NLong == tempLong + 1);
                        tempLong++;
                    }
                    turn = !turn;
                    Assert.IsTrue(difficulty.MaxSpeed > tempSpeed);
                }
                difficulty.Increase();
                Assert.IsTrue(difficulty.NLong == tempLong);
                Assert.IsTrue(difficulty.NShort == tempShort);

            }



    }
}
