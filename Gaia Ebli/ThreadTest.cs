﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace TaskOOP
{
    class ThreadTest
    {
        private static readonly int SLEEP = 10_000;

        /**
         * Tests whether the score decreases every second.
         */
        public void testScore()
        {
            IScore score = new Score();
            score.SetPoints(100);
            Thread scoreThread = new Thread(score.Run);
            scoreThread.Start();
            Thread thread = new Thread(() => {
                Thread.Sleep(SLEEP);
                scoreThread.Interrupt();
                Console.WriteLine("SCORE: Expected: 190, Result: " + score.globalScore);
            });
            thread.Start();
        }

        /**
         * Tests whether the timer increases every tenth of second.
         */
        public void testGameTimer()
        {
            IGameTimer timer = new GameTimer();
            Thread timerThread = new Thread(timer.Run);
            timerThread.Start();
            Thread thread = new Thread(() => {
                Thread.Sleep(SLEEP);
                timerThread.Interrupt();
                Console.WriteLine("TIMER: Expected: 00 : 00 : 10 : 00, Result: " + timer.ToString());
                Console.WriteLine("(It's impossible to predict the perfect timing so this is a valuation)");
            });
            thread.Start();
        }

        /**
         * Tests "testScore" and "testGameTimer".
         */
        public static void Main(String[] args)
        {
            ThreadTest test = new ThreadTest();
            test.testScore();
            test.testGameTimer();
        }
    }
}
