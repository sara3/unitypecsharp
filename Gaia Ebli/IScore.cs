﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskOOP
{
    public interface IScore
    {
        int globalScore { get; set; }

        bool isPaused { get; set; }

        /**
         * Computes and adds points to the global score based on the number of typed letters.
         */
        void SetPoints(int typedLetters);

        /**
         * Increments the combo by one.
         */
        void IncCombo();

        /**
         * Resets the combo.
         */
        void ResetCombo();

        /**
         * Resets the global score and the combo.
         */
        void Reset();

        /**
         * Decreases the global score by one every second.
         * If the current set of words has ended, it pauses for a two seconds.
         */
        void Run();
    }
}
