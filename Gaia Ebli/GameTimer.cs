﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Text;
using System.Threading;

namespace TaskOOP
{
    class GameTimer : IGameTimer
    {
        private static readonly int ONE_TENTH_PAUSE = 100;
        private static readonly int MAX_HOURS = 23;
        private static readonly int MAX_MINUTES = 59;
        private static readonly int MAX_SECONDS = 59;
        private static readonly int MAX_TENTHS = 9;
        private readonly NumberFormatInfo nfi = new NumberFormatInfo();
        public bool isPaused { get; set; }
        public int hours { get; set; }
        public int minutes { get; set; }
        public int seconds { get; set; }
        public int tenths { get; set; }

        public void Reset()
        {
            this.hours = 0;
            this.minutes = 0;
            this.seconds = 0;
            this.tenths = 0;
        }

        public void Run()
        {
            while (Thread.CurrentThread.IsAlive)
            {
                try
                {
                    Thread.Sleep(ONE_TENTH_PAUSE);
                }
                catch
                {
                    Thread.CurrentThread.Interrupt();
                }
                this.CheckTenths();
                if (this.isPaused)
                {
                    try
                    {
                        Thread.Sleep(2000);
                    }
                    catch
                    {
                        Thread.CurrentThread.Interrupt();
                    }
                }
                this.isPaused = false; //bisogna notificare il timer che la pausa è finita
            }
        }

        public int CompareTo(IGameTimer other)
        {
                if (other == null) return 1;
                return (this.hours != other.hours) ? this.hours - other.hours : this.CompareMin(other);
        }

        public override bool Equals(Object obj)
        {
            if (this == obj)
            {
                return true;
            }
            if (obj == null)
            {
                return false;
            }
            if (GetType() != obj.GetType())
            {
                return false;
            }
            GameTimer other = (GameTimer)obj;
            return hours == other.hours && minutes == other.minutes && seconds == other.seconds && tenths == other.tenths;
        }

        public override int GetHashCode()
        {
            int prime = 31;
            int result = 1;
            result = prime * result + hours;
            result = prime * result + minutes;
            result = prime * result + seconds;
            result = prime * result + tenths;
            return result;
        }

        public override String ToString()
        {
            nfi.NumberDecimalDigits = 0;
            return this.hours.ToString("N", nfi) + " : " + this.minutes.ToString("N", nfi) + " : " + this.seconds.ToString("N", nfi) + " : " + this.tenths.ToString("N", nfi);
        }

        private void CheckHours()
        {
            if (this.hours == MAX_HOURS)
            {
                this.Reset();
            }
            else
            {
                this.hours++;
            }
        }

        private void CheckMinutes()
        {
            if (this.minutes == MAX_MINUTES)
            {
                this.minutes = 0;
                this.CheckHours();
            }
            else
            {
                this.minutes++;
            }
        }

        private void CheckSeconds()
        {
            if (this.seconds == MAX_SECONDS)
            {
                this.seconds = 0;
                this.CheckMinutes();
            }
            else
            {
                this.seconds++;
            }
        }

        private void CheckTenths()
        {
            if (this.tenths == MAX_TENTHS)
            {
                this.tenths = 0;
                this.CheckSeconds();
            }
            else
            {
                this.tenths++;
            }
        }

        private int CompareSec(IGameTimer t)
        {
            return (this.seconds != t.seconds) ? this.seconds - t.seconds : this.tenths - t.tenths;
        }

        private int CompareMin(IGameTimer t)
        {
            return (this.minutes != t.minutes) ? this.minutes - t.minutes : this.CompareSec(t);
        }
    }
}
