using Microsoft.VisualStudio.TestTools.UnitTesting;
using TaskOOP;

namespace ScoreTest
{
    [TestClass]
    public class ScoreTest
    {
        private static readonly int DEFAULT_POINTS = 100;
        private static readonly int WITH_COMBO = 1200;
        private static readonly int WITHOUT_COMBO = 200;
        private static readonly int DEFAULT_COMBO = 5;
        private readonly IScore score = new Score();

        /**
         * Tests the algorithm which computes the points to add to the general score.
         */
        [TestMethod]
        public void TestSetPoints()
        {
            score.Reset();
            score.SetPoints(DEFAULT_POINTS);
            Assert.Equals(WITHOUT_COMBO, score.globalScore);
        }

        /**
         * Tests the increment of the combo value.
         */
        [TestMethod]
        public void TestCombo()
        {
            score.Reset();
            score.SetPoints(DEFAULT_POINTS);
            for (int i = 0; i < DEFAULT_COMBO; i++)
            {
                score.IncCombo();
            }
            score.SetPoints(DEFAULT_POINTS);
            Assert.AreNotEqual(WITHOUT_COMBO, score.globalScore);
            Assert.AreEqual(WITHOUT_COMBO + WITH_COMBO, score.globalScore);
            score.ResetCombo();
            score.SetPoints(DEFAULT_POINTS);
            Assert.AreEqual(WITHOUT_COMBO + WITH_COMBO + WITHOUT_COMBO, score.globalScore);
            Assert.AreNotEqual(WITHOUT_COMBO + WITH_COMBO + WITH_COMBO, score.globalScore);
        }

        /**
         * Tests the reset function of the combo and of the score.
         */
        [TestMethod]
        public void TestReset()
        {
            score.Reset();
            score.SetPoints(DEFAULT_POINTS);
            Assert.AreEqual(WITHOUT_COMBO, score.globalScore);
            score.Reset();
            Assert.AreEqual(0, score.globalScore);
            score.IncCombo();
            score.SetPoints(DEFAULT_POINTS);
            Assert.AreEqual(WITHOUT_COMBO + WITHOUT_COMBO, score.globalScore);
        }
    }
}
