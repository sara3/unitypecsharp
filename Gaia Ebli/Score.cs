﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace TaskOOP
{
    public class Score : IScore
    {
        private static readonly int LETTER_VALUE = 2;
        private static readonly int DECREASING_VALUE = 1;
        private static readonly int ONE_SECOND_PAUSE = 1000;
        private int combo = 1;
        public bool isPaused { get; set; }
        public int globalScore { get; set; }

        public void SetPoints(int typedLetters)
        {
            this.globalScore += this.ComputePoints(typedLetters);
        }

        public void IncCombo()
        {
            this.combo++;
        }

        public void ResetCombo()
        {
            this.combo = 1;
        }

        public void Reset()
        {
            this.ResetGlobalScore();
            this.ResetCombo();
        }

        public void Run()
        {
            while (Thread.CurrentThread.IsAlive)
            {
                
                this.DecreaseScore();
                if (this.isPaused)
                {
                    try
                    {
                        Thread.Sleep(2000);
                    }
                    catch
                    {
                        Thread.CurrentThread.Interrupt();
                    }
                }
                this.isPaused = false;
                try
                {
                    Thread.Sleep(ONE_SECOND_PAUSE);
                }
                catch
                {
                    Thread.CurrentThread.Interrupt();
                }
            }
        }

        public override String ToString()
        {
            return this.globalScore.ToString();
        }

        private void DecreaseScore()
        {
            if (this.globalScore > 0)
            {
                this.globalScore -= DECREASING_VALUE;
            }
        }

        private int ComputePoints(int letters)
        {
            return letters * LETTER_VALUE * this.combo;
        }

        private void ResetGlobalScore()
        {
            this.globalScore = 0;
        }
    }
}
