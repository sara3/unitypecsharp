﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskOOP
{
    interface IGameTimer : IComparable<IGameTimer>
    {
        int hours { get; set; }

        int minutes { get; set; }

        int seconds { get; set; }

        int tenths { get; set; }

        bool isPaused { get; set; }

        /**
         * Resets the timer.
         */
        void Reset();

        /**
        * Sleeps for a tenth of second and then updates the timer.
        * If the current set of words has ended, it pauses for a two seconds.
        */
        public void Run();
    }
}
