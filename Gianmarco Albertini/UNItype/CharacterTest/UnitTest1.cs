﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UNItype;

namespace CharacterTest
{
    [TestClass]
    public class UnitTest1
    {
        static readonly ICharacter character = new CharacterImpl(new KeyValuePair<double, double>(0.0, 0.0));
        string currentWord = "first";
        static readonly char typed1 = 'f';
        static readonly char typed2 = 'i';
        static readonly char typed3a = 's';
        static readonly char typed3b = 'h';
        static readonly char typed4 = 't';
        readonly ISet<string> wordSet = new HashSet<string>()
        {
            "cow",
            "mummy",
            "scary",
            "loaf",
            "house"
        };
        

        [TestMethod]
        public void TestHitMethodCorrect()
        {
            Assert.IsTrue(character.Hit(typed1, currentWord, wordSet));
        }

        [TestMethod]
        public void TestHitMethodMistake()
        {
            Assert.IsFalse(character.Hit(typed2, currentWord, wordSet));
        }

        [TestMethod]
        public void TestHitMethodChangeWordCorrect1()
        {
            character.ChangeAiming(false);
            Assert.IsTrue(character.Hit(typed3a, currentWord, wordSet));
            Assert.IsTrue(character.IsAiming());
        }

        [TestMethod]
        public void TestHitMethodChangeWordCorrect2()
        {
            character.ChangeAiming(false);
            currentWord = "scary";
            Assert.IsTrue(character.Hit(typed3b, currentWord, wordSet));
            Assert.IsTrue(character.IsAiming());
        }

        [TestMethod]
        public void TestHitMethodChangeWordMistake()
        {
            character.ChangeAiming(false);
            Assert.IsFalse(character.Hit(typed4, currentWord, wordSet));
            Assert.IsFalse(character.IsAiming());
        }
    }
}
