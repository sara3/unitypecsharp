﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UNItype
{
    public interface ICharacter
    {
        KeyValuePair<double, double> GetLocation();

        void SetLocation(KeyValuePair<double, double> newPos);

        bool IsAiming();

        void ChangeAiming(bool aiming);

        bool Hit(char letter, string currentWord, ISet<string> wordSet);
    }
}
