﻿using System;
using System.Collections.Generic;

namespace UNItype
{
    public class CharacterImpl : ICharacter
    {
        private KeyValuePair<double, double> location;
        private bool aiming;

        public CharacterImpl(KeyValuePair<double, double> pos)
        {
            this.location = pos;
            this.aiming = true;
        }

        public KeyValuePair<double, double> GetLocation() 
        {
            return this.location;
        }

        public void SetLocation(KeyValuePair<double, double> newPos)
        {
            this.location = newPos;
        }

        public bool IsAiming()
        {
            return this.aiming;
        }

        public void ChangeAiming(bool aiming)
        {
            this.aiming = aiming;
        }

        public bool Hit(char letter, string currentWord, ISet<string> wordSet)
        {
            if (this.IsAiming())
            {
                if (this.IsCorrect(letter, currentWord))
                {
                    Console.WriteLine("The current word has been hit!");
                    return true;
                }
                else
                {
                    Console.WriteLine("Combo interrupted! The input was wrong.");
                    return false;
                }
            }
            else
            {
                if (this.CheckPresence(letter, wordSet))
                {
                    string correctWord = GetCorrectWord(letter, wordSet);
                    this.ChangeAiming(true);
                    this.Hit(letter, correctWord, wordSet);
                    return true;
                }
                else
                {
                    Console.WriteLine("Combo interrupted! The input was wrong.");
                    return false;
                }
            }
        }

        private bool IsCorrect(char letter,  string word)
        {
            if (word != null)
            {
                return word[0].Equals(letter);
            }
            else
            {
                Console.WriteLine("Error, active word not present in the current set");
                return false;
            }
        }

        private bool CheckPresence(char letter, ISet<string> wordSet)
        {
            foreach (var word in wordSet)
            {
                if(word[0].Equals(letter))
                {
                    return true;
                }
            }

            return false;
        }

        private string GetCorrectWord(char letter, ISet<string> wordSet)
        {
            foreach (var word in wordSet)
            {
                if (word[0].Equals(letter))
                {
                    return word;
                }
            }

            Console.WriteLine("Fatal error, expected word is missing.");
            return null;
        }
    }
}
