﻿using System;
using System.Collections.Generic;

namespace Sara_Bernabini
{
    public interface IWord
    {
        char FirstLetter
        {
            get;
            set;
        }

        string Word
        {
            get;
            set;
        }

        bool Active
        {
            get;
            set;
        }

        int Length
        {
            get;
            set;
        }

        int Typed
        {
            get;
            set;
        }
        
        double Speed
        {
            get;
            set;
        }
        
        KeyValuePair<double, double> Position
        {
            get;
            set;
        }

        double WordGetY();

        double WordGetX();

        char GetCharAt(int index);

    }
}
