﻿using System.Collections.Generic;

namespace Sara_Bernabini
{
    public class WordImpl : IWord
    {

        private char firstLetter;
        private string word;
        private bool isActive;
        private int length;
        private int typed;
        private double speed;
        private KeyValuePair<double, double> position;

        public WordImpl(string word, double speed, KeyValuePair<double, double> position)
        {
            firstLetter = word[0];
            this.word = word;
            isActive = false;
            length = this.word.Length;
            typed = 0;
            this.speed = speed;
            this.position = position;
        }



        public bool Active
        {
            get { return isActive; }
            set { isActive = value; }
        }

        public char FirstLetter
        {
            get { return firstLetter; }
            set { firstLetter = value; }
        }

        public int Length
        {
            get { return length; }
            set { length = value; }
        }

        public KeyValuePair<double, double> Position
        {
            get { return position; }
            set { position = value; }
        }

        public double Speed
        {
            get { return speed; }
            set { speed = value; }
        }

        public int Typed
        {
            get { return typed; }
            set { typed = value; }
        }

        public string Word
        {
            get { return word; }
            set { word = value; }
        }

        public char GetCharAt(int index)
        {
            return word[index];
        }

        public double WordGetX()
        {
            return position.Key;
        }

        public double WordGetY()
        {
            return position.Value;
        }
    }
}
