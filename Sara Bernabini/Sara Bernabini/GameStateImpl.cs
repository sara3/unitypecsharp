﻿using System.Collections.Generic;

namespace Sara_Bernabini
{
    public class GameStateImpl : IGameState
    {

        private readonly HashSet<IWord> wordSet;
        private readonly KeyValuePair<double, double> unicornPosition;
        private bool pause = false;

        public GameStateImpl(HashSet<IWord> wordSet)
        {
            this.wordSet = wordSet;
            unicornPosition = new KeyValuePair<double, double>(26.0, 100.0);
        }
        
        public bool PauseState
        {
                get { return pause; }
                set { pause = value; }
        }

        public bool isGameOver()
        {
            foreach (var i in wordSet)
            {
                if (i.Position.Value >= unicornPosition.Value)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
