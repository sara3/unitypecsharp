﻿namespace Sara_Bernabini
{
    public interface IGameState
    {
        bool isGameOver();
        bool PauseState
        {
            get;
            set;
        }
    }
}
