﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sara_Bernabini;
using System.Collections.Generic;

namespace GameState_Word_Test
{
    [TestClass]
    public class UnitTest1
    {
       static HashSet<IWord> set = new HashSet<IWord>()
            {
                new WordImpl("sky", 0.1, new KeyValuePair<double, double>(10.5, 0.0))
            };
        IGameState game = new GameStateImpl(set);

        [TestMethod]
        public void TestGameState1()
        {
            Assert.IsFalse(game.isGameOver());
            IWord w2 = new WordImpl("banana", 0.1, new KeyValuePair<double, double>(13.5, 110.0));
            set.Add(w2);
            Assert.IsTrue(game.isGameOver());
        }

        [TestMethod]
        public void TestWord1()
        {
            IWord w3 = new WordImpl("pancakes", 0.1, new KeyValuePair<double, double>(13.5, 10.0));
            Assert.AreEqual("pancakes", w3.Word);

            Assert.AreNotEqual('a', w3.FirstLetter);
            Assert.AreEqual('p', w3.FirstLetter);

            Assert.AreNotEqual('p', w3.GetCharAt(1));
            Assert.AreEqual('c', w3.GetCharAt(3));

            Assert.AreNotEqual(7, w3.Length);
            Assert.AreEqual(8, w3.Length);

            Assert.AreEqual(0, w3.Typed);
            w3.Typed = w3.Typed + 1;
            Assert.AreEqual(1, w3.Typed);

        }
    }
}
